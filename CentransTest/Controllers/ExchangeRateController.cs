﻿using CentransTest.DBContext;
using CentransTest.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentransTest.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace CentransTest.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/rate")]
    public class ExchangeRateController : Controller
    {
        private ExchangeRateService rateService;
        private IUrlHelper urlHelper;
        private readonly DataContext dataContext;

        public ExchangeRateController(DataContext _dataContext, IUrlHelper _urlHelper)
        {
            dataContext = _dataContext;
            this.urlHelper = _urlHelper;
            rateService = new ExchangeRateService(_dataContext);
        }
        [HttpGet]
        [Route("getRates")]
        public IEnumerable<ExchangeRate> Get()
        {
            return rateService.getExchangeRates();   
        }

        [HttpGet]
        [Route("getRateToBank")]
        public ExchangeRate ExchangeRateRequest(string date)
        {
            string userId = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var res = rateService.exchangeRateRequestToBank(date, userId);
            return res;
        }

        [HttpPost]
        [Route("register")]
        public ExchangeRate SaveExchangeRate(ExchangeRate rate)
        {
            return null;
        }

        [HttpPut("{id:int}")]
        public bool Put(int id, [FromBody] ExchangeRate rate)
        {
            string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var res = rateService.editRate(id, rate, userAsp);
            return res;
        }

        [HttpPost]
        [Route("deleteRate")]
        public bool DeleteRate([FromBody] ExchangeRate rate)
        {
            string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var res = rateService.deleteRate(rate, userAsp);
            return res;
        }

    }
}
