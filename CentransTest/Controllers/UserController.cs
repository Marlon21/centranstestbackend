﻿using CentransTest.DBContext;
using CentransTest.Models;
using CentransTest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CentransTest.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/user")]
    public class UserController : Controller
    {
        private UserService userService;
        private IUrlHelper urlHelper;
        private readonly DataContext dataContext;
        public UserController(DataContext _dataContext, IUrlHelper _urlHelper)
        {
            dataContext = _dataContext;
            this.urlHelper = _urlHelper;
            userService = new UserService(_dataContext);
        }
        [HttpGet]
        [Route("getUsers")]
        public IEnumerable<User> Get()
        {
            return userService.getUsers();
        }

        [HttpPost]
        [Route("register")]
        public User SaveUser([FromBody]User user)
        {
            string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var res = userService.addUser(user, userAsp);
            return null;
        }

        [HttpPut("{id:int}")]
        public bool Put(int id, [FromBody] ExchangeRate rate)
        {
            string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            //var res = userService.editRate(id, rate, userAsp);
            return false;
        }

        [HttpPost]
        [Route("deleteRate")]
        public bool DeleteRate([FromBody] ExchangeRate rate)
        {
            string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            //var res = User.deleteRate(rate, userAsp);
            return false;
        }

    }
}
