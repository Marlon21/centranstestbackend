﻿using CentransTest.DBContext;
using CentransTest.IdentityAuth;
using CentransTest.Models;
using CentransTest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CentransTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        //private readonly RoleManager<MyRole> _roleManager;
        private IPasswordHasher<ApplicationUser> _passwordHasher;
        private IConfiguration _configurationRoot;
        private ILogger<AuthController> _logger;
        private UserService _service;
        private readonly IUrlHelper urlHelper;

        public AuthController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IPasswordHasher<ApplicationUser> passwordHasher, IConfiguration configurationRoot,
            ILogger<AuthController> logger, DataContext context,  IUrlHelper _urlHelper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            //_roleManager = roleManager;
            _logger = logger;
            _passwordHasher = passwordHasher;
            _configurationRoot = configurationRoot;
            //BLL_DATA.Helper._dataContext = context;
            _service = new UserService(context);
            urlHelper = _urlHelper;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            //string userAsp = urlHelper.ActionContext.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.FirstOrDefault().Errors.FirstOrDefault());
            }
            var user = new ApplicationUser()
            {
                UserName = model.Username,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            //_userManager.RemovePasswordAsync(user);
            //_userManager.AddPasswordAsync(user,"dd");

            
            if (result.Succeeded)
            {
                //
                User _user = new User();
                _user.Email = model.Email;
                _user.UserName = model.Username;
                _user.Name = model.Name;
                _user.LastName = model.LastName;
                _user.PhoneNumber = model.PhoneNumber;
                _user.AspNetUserId = user.Id;
                _service.addUser(_user, "admin");

                return Ok(result);
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("errorMessage", error.Description);
                return BadRequest(ModelState.Values.FirstOrDefault().Errors.FirstOrDefault());
            }
            return Ok(result);
        }

        [ValidateForm]
        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> CreateToken([FromBody] LoginModel model)
        {
            try
            {
                //var user = await _userManager.FindByNameAsync(model.Email);
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    IdentityError error1 = new IdentityError { Code = "1", Description = "the Username doesn't exists" };
                    //return IdentityResult.Failed(new IdentityError { Code = "1", Description = "sdfsfs" });
                    return BadRequest(IdentityResult.Failed(error1));
                    //return Unauthorized();
                }
                if (_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password) == PasswordVerificationResult.Success)
                {
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    };
                    _logger.LogError($"error while creating token: {_configurationRoot["Tokens:Key"]}");
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configurationRoot["Tokens:Key"]));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(_configurationRoot["Tokens:Issuer"],
                        _configurationRoot["Tokens:Issuer"],
                        claims,
                        expires: DateTime.Now.AddDays(10),
                        signingCredentials: creds);

                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo,
                        username = user.UserName
                    });
                }
                IdentityError error = new IdentityError { Code = "2", Description = " The password is incorrect" };
                //return IdentityResult.Failed(new IdentityError { Code = "1", Description = "sdfsfs" });
                return BadRequest(IdentityResult.Failed(error));
            }
            catch (Exception ex)
            {
                _logger.LogError($"error while creating token: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, "error while creating token");
            }
        }
    }
}
