﻿using System;
using Microsoft.AspNetCore.Identity;
namespace CentransTest.IdentityAuth
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }
    }
}
