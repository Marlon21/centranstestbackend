﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CentransTest.Models
{
    [Table("ExchangeRates")]
    public class ExchangeRate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        public DateTime date { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal rate { get; set; }
        public string username { get; set; }
    }
}
