﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CentransTest.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }

        [Column("name", TypeName = "varchar(60)")]
        public string Name { get; set; }

        [Column("lastName", TypeName = "varchar(60)")]
        public string LastName { get; set; }

        [Column("email", TypeName = "varchar(60)")]
        public string Email { get; set; }

        [Column("username", TypeName = "varchar(60)")]
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string AspNetUserId { get; set; } 
        public bool active { get; set; }

    }
}
