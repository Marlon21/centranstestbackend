﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentransTest.Models;

namespace CentransTest.DBContext
{
    public class DataContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<ExchangeRate> ExchangeRate{ get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
