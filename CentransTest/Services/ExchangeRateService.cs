﻿using CentransTest.DBContext;
using CentransTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CentransTest.Services
{
    public class ExchangeRateService
    {
        public readonly DataContext dataContext;
        public ExchangeRateService(DataContext _dataContext)
        {
            dataContext = _dataContext;

        }
        public List<ExchangeRate> getExchangeRates()
        {
            return dataContext.ExchangeRate.ToList();
        }
        public ExchangeRate exchangeRateRequestToBank(string date, string userId)
        {
            ExchangeRate rate = new ExchangeRate();

            var user = dataContext.User.Where(u => u.AspNetUserId == userId).SingleOrDefault();
            //var rateString = GetExchangeRateFromBank(date);
            var rateString = "7.72312";

            if (rateString != null && rateString != "")
            {
                rate.rate = decimal.Parse(rateString);
                rate.date = DateTime.Parse(date);
                rate.username = user.UserName;

                dataContext.ExchangeRate.Add(rate);
                dataContext.SaveChanges();
            }

            return rate;
        }

        public string GetExchangeRateFromBank(string date)
        {
            string rate = "";
            //Consumo el ws SOAP de pioworld para obtener el special Code
            string soapResult = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx?WSDL");
            request.Method = "POST";
            request.Accept = "text/xml";
            request.UseDefaultCredentials = true;
            request.Host = "www.banguat.gob.gt";
            request.ContentType = "text/xml";
            request.UserAgent = "centrans";

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                     <soap:Body>
                        <TipoCambioRango xmlns=""http://www.banguat.gob.gt/variables/ws/"">
                            <fechainit>" + date + @"</fechainit>
                            <fechafin>" + date + @"</fechafin>
                        </TipoCambioRango>
                      </soap:Body></soap:Envelope>");

            //proceso el Request
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            //Proceso la respuesta
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();

                    //Convierto la respuesta en string que devuelve el server a Xml Document
                    XDocument doc = XDocument.Parse(soapResult);
                    XNamespace ns = "http://www.banguat.gob.gt/variables/ws/";

                    //utilizo Linq para navegar en los nodos del documento xml
                    var results = from result in doc.Descendants(
                                                XName.Get("venta", ns.NamespaceName)
                                                )
                                  select result.Value;

                    // var a = results.

                    rate = results.FirstOrDefault();
                }
            }
            return rate;
        }

        public bool editRate(int id, ExchangeRate rate, string userId)
        {
            var user = dataContext.User.Where(u => u.AspNetUserId == userId).SingleOrDefault();
            var exchangeRate = dataContext.ExchangeRate.Find(id);

            if (exchangeRate != null)
            {
                exchangeRate.rate = rate.rate;
                exchangeRate.username = user.UserName;
                dataContext.ExchangeRate.Update(exchangeRate);
                dataContext.SaveChanges();

                return true;
            }
            return false;
        }

        public bool deleteRate(ExchangeRate rate, string userId)
        {
            var user = dataContext.User.Where(u => u.AspNetUserId == userId).SingleOrDefault();
            var exchangeRate = dataContext.ExchangeRate.Find(rate.id);

            dataContext.ExchangeRate.Remove(exchangeRate);
            var res = dataContext.SaveChanges();

            if (res == 1)
            {
                return true;
            }

            return false;
        }
    }
}
