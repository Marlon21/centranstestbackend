﻿using CentransTest.DBContext;
using CentransTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentransTest.Services
{
    public class UserService
    {
        public readonly DataContext dataContext;
        public UserService(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public List<User> getUsers()
        {
            return dataContext.User.ToList();
        }
        public User getUser(long id)
        {
            var book = dataContext.User.FirstOrDefault(b => b.Id == id);
            return book;
        }
        public IEnumerable<User> getIdUser(int id)
        {
            var status = dataContext.User.Where(l => l.Id == id).
                Select(l => new User { Id = l.Id, Name = l.Name, LastName = l.LastName });

            return status;
        }
        
        public bool addUser(User user, string userAsp)
        {
            try
            {
                var userId = dataContext.User.Where(u => u.AspNetUserId == userAsp).SingleOrDefault();
                user.active = true;
                dataContext.User.Add(user);
                int res = dataContext.SaveChanges();

                if (!res.Equals(1))
                {
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }
        public int editUser(int id, User user, string userAsp)
        {
            var userId = dataContext.User.Where(u => u.AspNetUserId == userAsp).SingleOrDefault();

            int res = 0;
            var _user = dataContext.User.Find(id);
            if (_user != null)
            {
                _user.Email = user.Email;
                _user.Name = user.Name;
                _user.LastName = user.LastName;
                _user.PhoneNumber = user.PhoneNumber;
                dataContext.User.Update(_user);
                res = dataContext.SaveChanges();

                //Guardando log
                /*ActionLogIcarus log = new ActionLogIcarus();
                log.logCode = "3";
                log.description = "Se ha Editado un usuario: " + user.UserName + ", Nuevos valores = name: " + user.Name + " " + user.LastName + ", tel:" + user.PhoneNumber + ", roleId: " + user.RoleID;
                log.userId = userId.Id;
                log.actionDate = DateTime.Now;
                log.mjRole = true;
                log.mjSuperAdministrator = true;
                icarusDataContext.ActionLogIcarus.Add(log);
                dataContext.SaveChanges();*/
            }

            return res;
        }
    }
}
